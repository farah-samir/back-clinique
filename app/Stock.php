<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    
    protected $flliabble= ['nomid','prix','tva','nobrachat','montant'];
  


      public function achat()
      {
          return $this->belongsTo(Achat::class);
      }
      public function vendu()
      {
          return $this->belongsTo(Vendu::class);
      }

      public function categorie()
      {
          return $this->belongsTo(Categorie::class);
      }

      public function operations(){
        return $this->hasMany(Operation::class);
      }

}
