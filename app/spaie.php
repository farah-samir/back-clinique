<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spaie extends Model
{
    protected $fillable = [
        'specialite','salaire','prime','salaire_net','cin','prenom'
  
        ];

  
        public function salarie()
        {
            return $this->belongsTo(Salarie::class);
        }
}
