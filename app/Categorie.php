<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    
    protected $flliabble= ['nom'];
   
    public function achats(){
        return $this->hasMany(Achat::class);
      }

      public function Vendus(){
        return $this->hasMany(Vendu::class);
      }
      public function Stocks(){
        return $this->hasMany(Stock::class);
      }
}
