<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paiement extends Model
{
    protected $fillable = [
      'nomid','pharmacie','monatnt_operation','dates','montant_departement','montant_globale'
  
        ];
        

        public function operation()
        {
            return $this->belongsTo(Operation::class);
        }

    }
