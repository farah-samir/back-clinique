<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
   
             protected $fillable = [
                'genre','dates','mois','heure'
          
                ];
        
            
          
                public function salarie()
                {
                    return $this->belongsTo(Salarie::class);
                }
                public function analyse()
                {
                    return $this->belongsTo(Analyse::class);
                }
                public function stock()
                {
                    return $this->belongsTo(Stock::class);
                }

                public function paiements(){
                    return $this->hasMany(Paiement::class);
                  }

}
