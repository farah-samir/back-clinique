<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Editor;

class EditorController extends Controller
{
  public function index(){

$editor = Editor::orderBy('created_at', 'desc')->get();
echo json_encode($editor);

  }
  public function show($editor_id){
    return Editor::find($editor_id);
  }



public function store(Request $request){
    $editor = new Editor();
    $editor->nom = $request->input('nom');
    $editor->htmlContent = $request->input('htmlContent');

      $editor->save();
    echo json_encode($editor);

}

public function update(Request $request,  $editor_id){
    $editor =  Editor::find($editor_id);
    $editor->nom = $request->input('nom');
    $editor->htmlContent = $request->input('htmlContent');
      $editor->save();
    echo json_encode($editor);

}

               public function destroy($editor_id){
               $editor = Editor::find($editor_id);
                      $editor->delete();

}

}
