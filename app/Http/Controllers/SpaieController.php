<?php

namespace App\Http\Controllers;
use App\Salarie;
use App\Spaie;

use Illuminate\Http\Request;

class SpaieController extends Controller
{
    public function index(){

        $spaie = Spaie::orderBy('created_at', 'desc')->with('salarie')->get();
        echo json_encode($spaie);
        
          }
          public function show($spaie_id){
            return Spaie::find($spaie_id);
          }
        
        
        
        public function store(Request $request){
            $spaie = new Spaie();
            $spaie->salarie_id = $request->input('salarie_id');
            $spaie->specialite = $request->input('specialite');
            $spaie->salaire = $request->input('salaire');
            $spaie->prime = $request->input('prime');
            $spaie->salaire_net = $request->input('salaire_net');
            $spaie->cin = $request->input('cin');
            $spaie->prenom = $request->input('prenom');
           
              $spaie->save();
            echo json_encode($spaie);
        
        }
        
        public function update(Request $request,  $spaie_id){
            $spaie =  Spaie::find($spaie_id);
            $spaie->salarie_id = $request->input('salarie_id');
            $spaie->specialite = $request->input('specialite');
            $spaie->salaire = $request->input('salaire');
            $spaie->prime = $request->input('prime');
            $spaie->salaire_net = $request->input('salaire_net');
            $spaie->cin = $request->input('cin');
            $spaie->prenom = $request->input('prenom');
           
        
        
              $spaie->save();
            echo json_encode($spaie);
        
        }
        
                       public function destroy($spaie_id){
                       $spaie = Spaie::find($spaie_id);
                              $spaie->delete();
        
        }
}
