<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Operation;
use App\Paiement;

class PaiementController extends Controller
{
    public function index(){

        $paiement = Paiement::orderBy('created_at', 'desc')->with('operation')->get();
        echo json_encode($paiement);
        
          }
     public function show($paiement_id){
            return Paiement::find($paiement_id);
          }
        
        
        
     public function store(Request $request){
            $paiement = new Paiement();
            $paiement->operation_id = $request->input('operation_id');
            $paiement->pharmacie = $request->input('pharmacie');
            $paiement->nomid = $request->input('nomid');
            $paiement->monatnt_operation = $request->input('monatnt_operation');
            $paiement->dates = $request->input('dates');
            $paiement->montant_departement = $request->input('montant_departement');
            $paiement->montant_globale = $request->input('montant_globale');


        
              $paiement->save();
            echo json_encode($paiement);
        
        }
        
     public function update(Request $request,  $paiement_id){
            $paiement =  Paiement::find($paiement_id);
            $paiement->operation_id = $request->input('operation_id');
            $paiement->pharmacie = $request->input('pharmacie');
            $paiement->nomid = $request->input('nomid');

            $paiement->monatnt_operation = $request->input('monatnt_operation');
            $paiement->dates = $request->input('dates');
            $paiement->montant_departement = $request->input('montant_departement');
            $paiement->montant_globale = $request->input('montant_globale');


        
              $paiement->save();
            echo json_encode($paiement);
        
        }
        
    public function destroy($paiement_id){
                       $paiement = Achat::find($paiement_id);
                              $paiement->delete();
        
        }
}
