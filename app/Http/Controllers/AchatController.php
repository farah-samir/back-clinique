<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorie;
use App\Achat;

class AchatController extends Controller
{
    public function index(){

        $achat = Achat::orderBy('created_at', 'desc')->with('categorie')->get();
        echo json_encode($achat);
        
          }
     public function show($achat_id){
            return Achat::find($achat_id);
          }
        
        
        
     public function store(Request $request){
            $achat = new Achat();
            $achat->nom = $request->input('nom');
            $achat->categorie_id = $request->input('categorie_id');
            $achat->prix = $request->input('prix');
            $achat->nombre = $request->input('nobrachat');

           
        
              $achat->save();
            echo json_encode($achat);
        
        }
        
     public function update(Request $request,  $achat_id){
            $achat =  Achat::find($achat_id);
            $achat->nom = $request->input('nom');
            $achat->categorie_id = $request->input('categorie_id');
            $achat->prix = $request->input('prix');
            $achat->nombre = $request->input('nobrachat');

      
              $achat->save();
            echo json_encode($achat);
        
        }
        
    public function destroy($achat_id){
                       $achat = Achat::find($achat_id);
                              $achat->delete();
        
        }
}
