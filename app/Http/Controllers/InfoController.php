<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Info;


class InfoController extends Controller
{
    public function index(){

        $info = Info::orderBy('created_at', 'desc')->get();
        echo json_encode($info);
        
          }
     public function show($info_id){
            return Info::find($info_id);
          }
        
        
        
     public function store(Request $request){
            $info = new Info();
            $info->cin = $request->input('cin');
            $info->prenom = $request->input('prenom');
            $info->nom = $request->input('nom');
            $info->adresse = $request->input('adresse');
            $info->telephone = $request->input('telephone');
            $info->telephone_mb = $request->input('telephone_mb');
            $info->date_red_v = $request->input('date_red_v');
    
            
              $info->save();
            echo json_encode($info);
        
        }
        
     public function update(Request $request,  $info_id){
            $info =  Info::find($info_id);
            $info->cin = $request->input('cin');
            $info->prenom = $request->input('prenom');
            $info->nom = $request->input('nom');
            $info->adresse = $request->input('adresse');
            $info->telephone = $request->input('telephone');
            $info->telephone_mb = $request->input('telephone_mb');
            $info->date_red_v = $request->input('date_red_v');
      
              $info->save();
            echo json_encode($info);
        
        }
        
    public function destroy($info_id){
                       $info = Info::find($info_id);
                              $info->delete();
        
        }
}
