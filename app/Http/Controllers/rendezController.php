<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Info;
use App\Rendez;
class rendezController extends Controller
{
    public function index(){

        $rendez = Rendez::orderBy('created_at', 'desc')->with('info')->get();
        echo json_encode($rendez);
        
          }
     public function show($rendez_id){
            return Rendez::find($rendez_id);
          }
        
        
     public function store(Request $request){
            $rendez = new Rendez();
            $rendez->info_id = $request->input('cin');
            $rendez->prenom = $request->input('prenom');
            $rendez->nom = $request->input('nom');
            $rendez->date_red_v = $request->input('date_red_v');
            $rendez->heure = $request->input('heure');
            $rendez->analyses = $request->input('analyses');
          
    
            
              $rendez->save();
            echo json_encode($rendez);
        
        }
        
     public function update(Request $request,  $rendez_id){
            $rendez =  Rendez::find($rendez_id);
            $rendez->info_id = $request->input('cin');
            $rendez->prenom = $request->input('prenom');
            $rendez->nom = $request->input('nom');
            $rendez->date_red_v = $request->input('date_red_v');
            $rendez->heure = $request->input('heure');
            $rendez->analyses = $request->input('analyses');
      
              $rendez->save();
            echo json_encode($rendez);
        
        }
        
    public function destroy($rendez_id){
                       $rendez = Rendez::find($rendez_id);
                              $rendez->delete();
        
        }
}
