<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Analyse;
use App\Depart;
use App\Rendez;
use App\Info;
class AnalyseController extends Controller
{

    public function index(){

        $analyse = Analyse::orderBy('created_at', 'desc')->with('depart')->with('rendez')
      
        ->get();
        echo json_encode($analyse);
        
          }
     public function show($analyse_id){
            return Analyse::find($analyse_id);
          }
        
        
        
     public function store(Request $request){
            $analyse = new Analyse();
            $analyse->depart_id = $request->input('depart_id');
            $analyse->nomid = $request->input('nomid');
            $analyse->rendez_id = $request->input('rendez_id');
            $analyse->chambre = $request->input('chambre');
            $analyse->test = $request->input('test');
            $analyse->date_red_v = $request->input('date_red_v');

           
            
              $analyse->save();
            echo json_encode($analyse);
        
        }
        
     public function update(Request $request,  $analyse_id){
            $analyse =  Analyse::find($analyse_id);
            $analyse->depart_id = $request->input('depart_id');
            $analyse->nomid = $request->input('nomid');
            $analyse->rendez_id = $request->input('rendez_id');
            $analyse->chambre = $request->input('chambre');
            $analyse->test = $request->input('test');
            $analyse->date_red_v = $request->input('date_red_v');

           
            
              $analyse->save();
            echo json_encode($analyse);
        }
        
    public function destroy($analyse_id){
                       $analyse = Analyse::find($analyse_id);
                              $analyse->delete();
        
        }


}
