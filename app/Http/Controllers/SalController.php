<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Salarie;
use App\Sal;
class SalController extends Controller
{
    public function index(){

        $sal = Sal::orderBy('created_at', 'desc')->with('salarie')->get();
        echo json_encode($sal);
        
        
        
        
        
          }
          public function show($sal_id){
            return Sal::find($sal_id);
          }
        
        
        
        public function store(Request $request){
            $sal = new Sal();
            $sal->salarie_id = $request->input('salarie_id');
            $sal->date_e = $request->input('date_e');
            $sal->date_s = $request->input('date_s');
            $sal->genreconge = $request->input('genreconge');
            $sal->congeformation = $request->input('congeformation');
            $sal->congecitoyen = $request->input('congecitoyen');
            $sal->prenom = $request->input('prenom');
            $sal->specialite = $request->input('specialite');
          
           
        
              $sal->save();
            echo json_encode($sal);
        
        }
        
        public function update(Request $request,  $sal_id){
            $sal =  Sal::find($sal_id);
            $sal->salarie_id = $request->input('salarie_id');
            $sal->date_e = $request->input('date_e');
            $sal->date_s = $request->input('date_s');
            $sal->genreconge = $request->input('genreconge');
            $sal->congeformation = $request->input('congeformation');
            $sal->congecitoyen = $request->input('congecitoyen');
            $sal->prenom = $request->input('prenom');
            $sal->specialite = $request->input('specialite');
        
        
              $sal->save();
            echo json_encode($sal);
        
        }
        
                       public function destroy($sal_id){
                       $sal = Sal::find($sal_id);
                              $sal->delete();
        
        }
}
