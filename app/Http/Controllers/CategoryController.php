<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorie;

class CategoryController extends Controller
{
    public function index(){

        $categorie = Categorie::orderBy('created_at', 'desc')->get();
        echo json_encode($categorie);
        
          }
     public function show($categorie_id){
            return Categorie::find($categorie_id);
          }
        
        
        
     public function store(Request $request){
            $categorie = new Categorie();
            $categorie->nom = $request->input('nom');
           
        
              $categorie->save();
            echo json_encode($categorie);
        
        }
        
     public function update(Request $request,  $categorie_id){
            $categorie =  Categorie::find($categorie_id);
            $categorie->nom = $request->input('nom');
      
              $categorie->save();
            echo json_encode($categorie);
        
        }
        
    public function destroy($categorie_id){
                       $categorie = Categorie::find($categorie_id);
                              $categorie->delete();
        
        }
}
