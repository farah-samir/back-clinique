<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorie;
use App\Achat;
use App\Stock;

class stockController extends Controller
{
    public function index(){

        $stock = Stock::orderBy('created_at', 'desc')->with('achat')->with('Categorie')->get();
        echo json_encode($stock);
        
          }
     public function show($stock_id){
            return Stock::find($stock_id);
          }
        
        
        
     public function store(Request $request){
            $stock = new Stock();
            $stock->nomid = $request->input('nomid');
            $stock->achat_id = $request->input('achat_id');
            $stock->categorie_id = $request->input('categorie_id');
            $stock->prix = $request->input('prix');
            $stock->tva = $request->input('tva');
            $stock->nobrachat = $request->input('nobrachat');
            $stock->montant = $request->input('montant');

            
              $stock->save();
            echo json_encode($stock);
        
        }
        
     public function update(Request $request,  $stock_id){
            $stock =  Stock::find($stock_id);
            $stock->nomid = $request->input('nomid');
            $stock->achat_id = $request->input('achat_id');
            $stock->categorie_id = $request->input('categorie_id');
            $stock->prix = $request->input('prix');
            $stock->tva = $request->input('tva');
            $stock->nobrachat = $request->input('nobrachat');
            $stock->montant = $request->input('montant');
      
              $stock->save();
            echo json_encode($stock);
        
        }
        
    public function destroy($stock_id){
                       $stock = Stock::find($stock_id);
                              $stock->delete();
        
        }
}
