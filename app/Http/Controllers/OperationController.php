<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Analyse; 
use App\Salarie;
use App\Stock;
use App\Operation;

class OperationController extends Controller
{

    public function index(){

        $operation = Operation::orderBy('created_at', 'desc')->with('salarie')->with('analyse')->with('stock')->get();
        echo json_encode($operation);
        
          }
     public function show($operation_id){
            return Operation::find($operation_id);
          }
        
        
        
     public function store(Request $request){
            $operation = new Operation();
            $operation->salarie_id = $request->input('nom_docteur');
            $operation->analyse_id = $request->input('nom_maladie');
            $operation->genre = $request->input('genre');
            $operation->dates = $request->input('dates');
            $operation->stock_id = $request->input('pharmacie');
            $operation->mois = $request->input('mois');
            $operation->heure = $request->input('heure');
    
            
              $operation->save();
            echo json_encode($operation);
        
        }
        
     public function update(Request $request,  $operation_id){
            $operation =  Operation::find($operation_id);
            $operation->salarie_id = $request->input('nom_docteur');
            $operation->analyse_id = $request->input('nom_maladie');
            $operation->genre = $request->input('genre');
            $operation->dates = $request->input('dates');
            $operation->stock_id = $request->input('pharmacie');
            $operation->mois = $request->input('mois');
            $operation->heure = $request->input('heure');
    
            
              $operation->save();
            echo json_encode($operation);
        
        }
        
    public function destroy($operation_id){
                       $operation = Operation::find($operation_id);
                              $operation->delete();
        
        }



}
