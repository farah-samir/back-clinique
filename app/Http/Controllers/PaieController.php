<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Salarie;
use App\Paie;
class PaieController extends Controller
{
    public function index(){

        $paie = Paie::orderBy('created_at', 'desc')->with('salarie')->get();
        echo json_encode($paie);
        
          }
          public function show($paie_id){
            return Paie::find($paie_id);
          }
        
        
        
        public function store(Request $request){
            $paie = new Paie();
            $paie->salarie_id = $request->input('salarie_id');
            $paie->specialite = $request->input('specialite');
            $paie->salaire = $request->input('salaire');
            $paie->prime = $request->input('prime');
            $paie->salaire_net = $request->input('salaire_net');
            $paie->cin = $request->input('cin');
            $paie->prenom = $request->input('prenom');
           
              $paie->save();
            echo json_encode($paie);
        
        }
        
        public function update(Request $request,  $sal_id){
            $paie =  Paie::find($sal_id);
            $paie->salarie_id = $request->input('salarie_id');
            $paie->specialite = $request->input('specialite');
            $paie->salaire = $request->input('salaire');
            $paie->prime = $request->input('prime');
            $paie->salaire_net = $request->input('salaire_net');
            $paie->cin = $request->input('cin');
            $paie->prenom = $request->input('prenom');
           
        
        
              $paie->save();
            echo json_encode($paie);
        
        }
        
                       public function destroy($paie_id){
                       $paie = Paie::find($paie_id);
                              $paie->delete();
        
        }
}
