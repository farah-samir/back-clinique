<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Depart;
class DepartController extends Controller
{
    public function index(){

        $depart = Depart::orderBy('created_at', 'desc')->get();
        echo json_encode($depart);
        
          }
     public function show($depart_id){
            return Depart::find($depart_id);
          }
        
        
        
     public function store(Request $request){
            $depart = new Depart();
            $depart->nom_departement = $request->input('nom_departement');

            $depart->chambre = $request->input('chambre');
           
        
              $depart->save();
            echo json_encode($depart);
        
        }
        
     public function update(Request $request,  $depart_id){
            $depart =  Depart::find($depart_id);
            $depart->nom_departement = $request->input('nom_departement');

            $depart->chambre = $request->input('chambre');
      
              $depart->save();
            echo json_encode($depart);
        
        }
        
    public function destroy($depart_id){
                       $depart = Depart::find($depart_id);
                              $depart->delete();
        
        }
}
