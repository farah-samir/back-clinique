<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jadwal;

class JadwalController extends Controller
{
    public function index(){

        $jadwal = Jadwal::orderBy('created_at', 'desc')->get();
        echo json_encode($jadwal);
        
          }
          public function show($jadwal_id){
            return Jadwal::find($jadwal_id);
          }
        
        
        
        public function store(Request $request){
            $jadwal = new Jadwal();
            $jadwal->start = $request->input('start');
            $jadwal->title = $request->input('title');
        
              $jadwal->save();
            echo json_encode($jadwal);
        
        }
        
        public function update(Request $request,  $jadwal_id){
            $jadwal =  Jadwal::find($jadwal_id);
            $jadwal->start = $request->input('start');
            $jadwal->title = $request->input('title');
              $jadwal->save();
            echo json_encode($jadwal);
        
        }
        
                       public function destroy($jadwal_id){
                       $jadwal = Jadwal::find($jadwal_id);
                              $jadwal->delete();
        
        }
}
