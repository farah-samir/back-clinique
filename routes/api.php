<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::resource('/log','logController');
Route::resource('/home','homeController');
Route::resource('/conge','CongeController');
Route::resource('/full','fullController');
Route::resource('/editor','EditorController');
Route::resource('/jadwal','JadwalController');
Route::resource('/categorie','CategoryController');
Route::resource('/achat','AchatController');
Route::resource('/vendu','VenduController');
Route::resource('/stock','StockController');
Route::resource('/info','infoController');
Route::resource('/rendez','rendezController');
Route::resource('/departement','DepartController');
Route::resource('/analyse','AnalyseController');
Route::resource('/salarie','SalarieController');
Route::resource('/sal','SalController');
Route::resource('/paie','PaieController');
Route::resource('/spaie','SpaieController');
Route::resource('/operation','OperationController');
Route::resource('/paiement','PaiementController');
Auth::routes();


Route::group([
    'middleware' => 'api',
], function () {

    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('sendPasswordResetLink', 'ResetPasswordController@sendEmail');
    Route::post('resetPassword', 'ChangePasswordController@process');
});
