<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('salarie_id');
            $table->unsignedInteger('analyse_id');
            $table->unsignedInteger('stock_id');
            $table->string('genre'); 
            $table->date('dates');
             $table->string('mois');
            $table->string('heure');
         
            $table->timestamps();

    



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
