<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpaiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spaies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('salarie_id');

            $table->string('specialite'); 
            $table->integer('salaire');
             $table->integer('prime');
            $table->integer('salaire_net');
            $table->string('cin');
            $table->string('prenom');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spaies');
    }
}
