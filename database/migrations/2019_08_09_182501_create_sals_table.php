<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('salarie_id');
            $table->date('date_s');
            $table->date('date_e');
          
            $table->string('genreconge');
          $table->string('congeformation');
          
         $table->string('congecitoyen');
          $table->string('specialite');
          $table->string('prenom');






            $table->timestamps();
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sals');
    }
}
